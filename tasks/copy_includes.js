/*
 * grunt-copy-includes
 * 
 *
 * Copyright (c) 2015 Luka Zitnik
 * Licensed under the MIT license.
 */

'use strict';

var cheerio = require('cheerio'),
    path = require('path'),
    css = require('css');

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  function extractCssUrls(ast, relativeTo) {

    var cssUrls = [],
        pattern = /url\(['"]?([^)]+?)['"]?\)/g,
        match,
        i,
        j;

    for (i = 0; i < ast.stylesheet.rules.length; ++i) {

      if (['comment', 'keyframes'].indexOf(ast.stylesheet.rules[i].type) !== -1) {
        continue;
      }

      for (j = 0; j < ast.stylesheet.rules[i].declarations.length; ++j) {
        switch (ast.stylesheet.rules[i].declarations[j].property) {
          case 'background-image':
          case 'background':
          case 'cursor':
          case 'src':
          case 'list-style-image':
          case 'list-style':
          {
            while(match = pattern.exec(ast.stylesheet.rules[i].declarations[j].value)) {
              cssUrls.push(path.join(relativeTo, match[1]));
            }
          }
        }
      }
    }

    return cssUrls;
  }

  function extractStyleLinks($, relativeTo) {

    var styleLinks = [];

    $('link[rel=stylesheet][href]').each(function() {

      var filepath = path.join(relativeTo, this.attribs.href),
          fileContents = grunt.file.read(filepath),
          ast = css.parse(fileContents);

      styleLinks.push(filepath);
      styleLinks = styleLinks.concat(extractCssUrls(ast, path.dirname(filepath)));
    });

    return styleLinks;
  }

  function extractScriptReferences($, relativeTo) {

    var scriptReferences = [];

    $('script[src]').each(function() {
      scriptReferences.push(path.join(relativeTo, this.attribs.src));
    });

    return scriptReferences;
  }

  function extractIncludes($, relativeTo) {
    return extractScriptReferences($, relativeTo).concat(extractStyleLinks($, relativeTo));
  }

  grunt.registerMultiTask('copy_includes', 'Copy external resources of your index file to selected directory.', function() {

    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({});

    // Iterate over all specified file groups.
    this.files.forEach(function(f) {

      var includes = [];

      f.src.filter(function(filepath) {

        // Warn on and remove invalid source files (if nonull was set).
        if (!grunt.file.exists(filepath)) {
          grunt.log.warn('Source file "' + filepath + '" not found.');
          return false;
        } else {
          return true;
        }
      }).forEach(function(filepath) {

        var srcContents = grunt.file.read(filepath),
            $ = cheerio.load(srcContents);

        includes.push(filepath);
        includes = includes.concat(extractIncludes($, path.dirname(filepath)));
      });

      includes.forEach(function(filepath) {
        if (!grunt.file.exists(filepath)) {
          grunt.verbose.error('Source file "' + filepath + '" not found.');
          return;
        }

        grunt.file.copy(filepath, path.join(f.dest, filepath));
      });

    });
  });

};
