# grunt-copy-includes

> Copy external resources of your index file to selected directory.

## Overview
*Includes* and *external resources* refer here to

* scripts specified by script elements,
* stylesheets linked by link elements and subsequently,
* background images of elements on arbitrary number of layers,
* font files with fallback options,
* cursor images with fallback options and
* list item marker images.

All on relative paths.

If you are building your web thing from modules like Bower components, you can let this Grunt plugin copy these types of resources, and only those you used, to your desired directory for packaging.

### Usage Example
```js
grunt.initConfig({
  copy_includes: {
    default: {
      src: 'index.html',
      dest: 'build'
    }
  }
});
```

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-copy-includes --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-copy-includes');
```

## Options
There are no task-specific configuration options available.

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History

<table>
  <tr>
    <td>2015-02-09</td>
    <td>v0.1.2</td>
    <td>Fixed repo url</td>
  </tr>
  <tr>
    <td>2015-02-09</td>
    <td>v0.1.1</td>
    <td>Going for HTML table in this Markdown file</td>
  </tr>
  <tr>
    <td>2015-02-06</td>
    <td>v0.1.0</td>
    <td>First release to npm</td>
  </tr>
</table>
