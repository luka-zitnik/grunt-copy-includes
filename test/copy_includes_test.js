'use strict';

var grunt = require('grunt');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

exports.copy_includes = {
  setUp: function(done) {
    // setup here if necessary
    done();
  },
  overallTest: function(test) {
    test.expect(3);

    var expected = [ 'index.html',
      'js',
      'js/app.js',
      'style',
      'style/font',
      'style/font/FiraSans-Bold.ttf',
      'style/img',
      'style/img/cross_stripes.svg',
      'style/img/honeycomb.svg',
      'style/style.css' ];

    test.deepEqual(grunt.file.expand({cwd: 'tmp/compactMapping/test/fixtures'}, '**/*'), expected);
    test.deepEqual(grunt.file.expand({cwd: 'tmp/filesObjectMapping/test/fixtures'}, '**/*'), expected);
    test.deepEqual(grunt.file.expand({cwd: 'tmp/filesArrayMapping/test/fixtures'}, '**/*'), expected);

    test.done();
  }
};
